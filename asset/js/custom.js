$(function(){
  $('.sidebar').slick({
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  vertical:true,
  autoplay: true,
  autoplaySpeed: 2000,
  arrows:true,
  prevArrow: $('.prev'),
  nextArrow: $('.next'),
  });

    $('#summernote').summernote({
      height:80
    });
});