<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Q & A A</title>
    <link rel="stylesheet" href="asset/css/bootstrap.min.css">
    <link rel="stylesheet" href="asset/css/slick.css">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">

    <link rel="stylesheet" href="asset/css/style.css">
    <script type="text/javascript" src="asset/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="asset/js/slick.min.js"></script>
    <script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>
    <script type="text/javascript" src="asset/js/custom.js"></script>
</head>

<body>
<section>
    <div class="container">
        <div class="row">
            <div class="header">
                <div class="phone"><span class="glyphicon glyphicon-phone"></span>
                    <p>017xxxxxxxx</p>
                </div>
                <div class="user"><span class="glyphicon glyphicon-user"></span><a href="signup.php">sign-up</a>
                </div>
                <div class="link"><span class="glyphicon glyphicon-link"></span><a href="login.php">login</a>
                </div>
                <div class="ques"><span class="glyphicon glyphicon-question-sign"></span><a href="massege.php">Ask Question</a></div>
                <div class="home"><span class="glyphicon glyphicon-home"></span><a href="index.php">home</a></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="manu">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbrand" href="#"><img src="asset/images/qa1.png" alt="logo"></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Dropdown
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                            </ul>
                            <form class="navbar-form pull-right">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                </div>
                                <button type="submit" class="btn btn-default">Submit</button>
                            </form>

                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>
</section>
<section id="mainBody">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="maincontent">
                    <p>ask question here??</p><span class="glyphicon glyphicon-triangle-bottom"></span>
                    <form action="quesstore.php" method="post">
                        <textarea name="summernote" id="summernote" cols="30" rows="10"></textarea>
                        <input type="submit" value="submit">
                    </form>
                </div>
            </div>
            <div class="col-sm-4">
                <h2 class="text-center sid_txt">most viewed question</h2>
                <div class="sidebar">
                    <p>Lorem ipsum dolor sit amet?</p>
                    <p>Lorem ipsum dolor sit amet?</p>
                    <p>Lorem ipsum dolor sit amet?</p>
                    <p>Lorem ipsum dolor sit amet?</p>
                    <p>Lorem ipsum dolor sit amet?</p>
                </div>
                <div class="arrows">
                    <span class="glyphicon glyphicon-chevron-left next"></span>
                    <span class="glyphicon glyphicon-chevron-right prev"></span>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="footer text-center">
                <p>&copy; all right reserved by Tanvir</p>
            </div>
        </div>
    </div>
</section>
</body>

</html>